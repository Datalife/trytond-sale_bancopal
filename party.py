# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval
from trytond.modules.company.model import CompanyValueMixin

__all__ = ['Party', 'BancopalPriceList']


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    bancopal = fields.Boolean('Bancopal')
    bancopal_price_list = fields.MultiValue(fields.Many2One(
            'product.price_list', "Bancopal Price List",
            domain=[
                ('company', '=', Eval('context', {}).get('company', -1)),
                ],
            states={
                'invisible': ~Eval('context', {}).get('company'),
                'required': Eval('bancopal', False),
                },
            depends=['bancopal']))
    bancopal_price_lists = fields.One2Many('party.party.bancopal_price_list',
        'party', 'Bancopal price lists')

    @staticmethod
    def default_bancopal():
        return False


class BancopalPriceList(ModelSQL, CompanyValueMixin):
    "Party Sale Price List"
    __name__ = 'party.party.bancopal_price_list'

    party = fields.Many2One(
        'party.party', "Party", ondelete='CASCADE', select=True)
    bancopal_price_list = fields.Many2One(
        'product.price_list', "Bancopal Price List",
        domain=[
            ('company', '=', Eval('company', -1)),
            ],
        depends=['company'])
