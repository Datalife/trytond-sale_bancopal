datalife_sale_bancopal
======================

The sale_bancopal module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_bancopal/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_bancopal)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
